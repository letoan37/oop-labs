/**
 * chuong trinh tim max(a,b), min cua Array, tinh BMI
 * @author Le Khanh Toan
 * @version 1.0
 * @since 2018-09-15
 * //MARK: CHECKED
 */
public class OOP4 {

    /**
     * max la ham tim max cua 2 so nguyen
     * @param a la so nguyen 1
     * @param b la so nguyen 2
     * @return int tra ve so lon nhat trong 2 so a,b
     */
    public int max(int a, int b)
    {
        if (a > b) {
            return a;
        } else {
            return b;
        }
    }
    /**
     * minArr la ham tim min trong 1 mang
     * @param n la so phan tu cua mang
     * @param a la mang can tim min
     * @return int tra ve so nho nhat trong mang
     */
    public int minArr(int n, int[] a)
    {
        int min = a[0];
        for(int i = 0; i < a.length; i++){
            if (i < min) {
                min = i;
            }
        }
        return min;
    }
    /**
     * checkBmi la ham tinh chi so Bmi roi nhan xet
     * @param weight la can nang
     * @param  height la chieu cao
     * @return String tra ve nhan xet voi tung chi so bmi
     */
    public String checkBMI(double weight, double height)
    {
         double BMI = weight/(height*height);
         if (BMI < 18.5) return "Thieu can";
         if (BMI < 22.99) return "Binh thuong";
         if (BMI < 24.99) return "Thua can";
         return "Beo phi";
    }

}
