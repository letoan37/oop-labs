/**
 * Chương trình in dòng chữ Hello World ra màn hình
 * @author Lê Khánh Toàn
 * @version 1.0
 * @since 2018-09-04
 */
public class helloworld
{
    public static void main(String[] args)
    {
        // In ra màn hình
        System.out.print("Hello World, Java!");
    }

}
