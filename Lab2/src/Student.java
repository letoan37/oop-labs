/**
 * class Student để quản lý sinh viên
 * @author Lê Khánh Toàn
 * @version 1.0
 * @since 2018-09-10
 */
public class Student
{
    private String id, name, email, group;
   
    /**
     * Hàm getInfo in ra thông tin của s/v
     */
    public void getInfo()
    {
        System.out.println("Ho ten: "+ this.name);
        System.out.printf("MSSV: "+ this.id);
        System.out.printf("Group: "+ this.group);
        System.out.printf("Email: "+ this.email);
    }
    /**
     * Hàm Student() khởi tạo
     */
    public Student()
    {
        setName("Student");
        setId("000");
        setGroup("K59CB");
        setEmail("uet@vnu.edu.vn");
    }
    /**
     * Hàm Student(n,idd,emaill) khởi tạo khi nhận biét được tên, id, email cúa s/v
     * @param n là tên sinh viên
     * @param idd là mssv cua sinh viên
     * @param emaill la email cua sinh viên
     */
    public Student(String n, String idd, String emaill)
    {
       
        setId(idd); setName(n);
        setGroup("K59CB");
        setEmail(emaill);
    }
    /**
     * Hàm Student(s) khởi tạo 
     * @param s là sinh viên cần sao chép
     */
    public Student(Student s)
    { setGroup(s.getGroup());
        setName(s.getName());
       
       
        setEmail(s.getEmail()); setId(s.getId());
    }

   
    public String getGroup()
    {
        return group;
    }
	 public void setGroup(String group)
    {
        this.group = group;
    }
    public String getEmail()
    {
        return email;
    }  public void setEmail(String email)
    {
        this.email = email;
    }
 public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }
 public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

   

  
}
