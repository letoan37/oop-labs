/**
 * class StudentManagement quản lý s/v
 * @author Lê Khánh Toàn
 * @version 1.0
 * @since 2018-09-10
 */
import OOP2.Student;

public class StudentManagement
{

    private Student[] student = new Student[1000];
    public  int count = 0;

    
    /**
     * studentByGroup là hàm in ra danh sách sinh viên theo group
     */
    public void studentByGroup()
    {
       
        int[] check = new int[count];
        for(int i = 0; i < this.count; i++)
            if (check[i]==0)
            {
                for (int j = i; j < this.count; j++)
                    if (this.student[i].getGroup()==this.student[j].getGroup())
                    {
                        check[j] = 1;
                        this.student[j].getInfo();
                    }
            }
    }
    /**
     * Hàm removeStudent de xoa mot sinh viên ra khoi danh sach
     * @param idd la mssv cua sinh viên do
     */
    public  void removeStudent(String idd)
    {

        for (int i = 0; i < this.count-1; i++)
            if (this.student[i].getId() == idd) {
         
        for (int j = i; i <this.count; j++) { 
            this.student[i] = this.student[i+1];
        }
	break;
	}
        count--;
    }
    /**
     * Hàm addStudent để add S/v vào danh sách
     * @param s la sinh viên được thêm
     */
    public  void addStudent(Student s)
    {
        this.student[this.count] = new Student();
        this.student[this.count] = s;
        count++;

    }

    public static void main(String[] args)
    {
        Student a = new Student("Toan", " 17021345", "lekhanhtoan37@gmail.com");
        StudentManagement p = new StudentManagement();
       
        a.setName("ABC");
        p.addStudent(a);
        a.setGroup("K59CB");
        a.setGroup("K59CLC");
        p.addStudent(a);
        p.addStudent(a);
        p.studentByGroup();

    }

}
