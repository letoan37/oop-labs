/**
 * class Rectangle
 * @author Le Khanh Toan
 * @version 1.0
 * @since 2018-10-09
 */
public class Rectangle extends Shape
{
    /**
     * Ham khoi tao mac dinh
     */
    public Rectangle()
    {
        super();
        setWidth(1.0);
        setHeight(1.0);
    }

    /**
     * Ham khoi tao chi co chieu dai, rong
     * @param _width chieu rong hcn
     * @param _length chieu dai hcn
     */
    public Rectangle(double _width, double _length)
    {
        super();
        setHeight(_length);
        setWidth(_width);
    }

    /**
     * Ham khoi tao day du tham so
     * @param _width chieu rong hcn
     * @param _length chieu dai hcn
     * @param _color mau sac hcn
     * @param _filled trang thai fill
     * @param layoutX toa do x
     * @param layoutY toa do y
     */
    public Rectangle(double _width, double _length, String _color, boolean _filled, int layoutX, int layoutY)
    {
        super(_color,_filled,layoutX,layoutY);
        setWidth(_width);
        setHeight(_length);

    }

    /**
     * Ham tinh dien tich hcn
     * @return double dien tich hcn
     */
    public double getArea()
    {
        return getHeight() * getWidth();
    }

    /**
     * Ham tinh chu vi hcn
     * @return double chu vi hcn
     */
    public double getPerimeter()
    {
        return 2 * (getWidth() + getHeight());
    }

    /**
     * Ham in thong tin hcn
     * @return String la thong tin gom chieu dai, rong, mau sac, loai hinh
     */
    @Override
    public String toString ()
    {
        return "Shape: Retangle\nWidth: " + getWidth() + "\nLength: " + getHeight() + "\n" + super.toString();
    }
}
