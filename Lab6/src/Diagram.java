/**
 * class Diagram
 * @author Le Khanh Toan
 * @version 1.0
 * @since 2018-10-09
 */

import java.util.LinkedList;

public class Diagram
{
    protected double width, height;

    protected LinkedList<Layer> listLayers = new LinkedList<Layer>();

    public double getWidth ()
    {
        return width;
    }
    public void setWidth (double width)
    {
        this.width = width;
    }

    public double getHeight ()
    {
        return height;
    }
    public void setHeight (double height)
    {
        this.height = height;
    }



    /**
     * constructor rong
     */
    public Diagram(){
        //
    };

    /**
     * constructor khoi tao chieu dai, rong
     * @param _width chieu rong
     * @param _height chieu dai
     */
    public Diagram(int _width, int _height)
    {
        setWidth(_width);
        setHeight(_height);
    }

    /**
     * constructor khoi tao nhieu layer
     * @param list danh sach layer
     * @param _width chieu rong
     * @param _height chieu dai
     */
    public Diagram(LinkedList<Layer> list, int _width, int _height)
    {
        setWidth(_width);
        setHeight(_height);
        listLayers.clear();
        listLayers.addAll(list);
    }

    /**
     * ham xoa tat ca doi tuong thuoc Retangle
     */
    public void removeAllTriangle()
    {
        for (int i = 0; i < listLayers.size(); i++)
        {
            for (int j = 0; j < listLayers.get(i).listShapes.size();j++)
                if (listLayers.get(i).listShapes.get(j) instanceof Triangle) {
                    listLayers.get(i).listShapes.remove(j);
                }
        }
    }

    /**
     * Ham in thong in
     * @return thong tin Diagram
     */
    @Override
    public String toString ()
    {
	String a = "";
        for(int i = 0; i < listLayers.size(); i++) {
            a += listLayers.get(i).toString();
        }

        return a;
    }
}
