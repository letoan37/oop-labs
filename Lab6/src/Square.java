/**
 * class Square ke thua Class Retangle
 * @author Le Khanh Toan
 * @version 1.0
 * @since 2018-10-09
 */

public class Square extends Rectangle
{
    public double getSide()
    {
        return getWidth();
    }
    public void setSide(double Side)
    {
        setHeight(Side);
        setWidth(Side);
    }
    @Override
    public void setHeight(double Side)
    {
        super.setHeight(Side);
        super.setWidth(Side);
    }
    @Override
    public void setWidth(double Side)
    {
        super.setWidth(Side);
        super.setHeight(Side);
    }
    /**
     * Constructor rong
     */
    public Square()
    {
        super();
    }

    /**
     * Constructor
     * @param Side canh cua hinh vuong
     */
    public Square(double Side)
    {
        super(Side,Side);
    }

    /**
     * constructor
     * @param Side canh cua hv
     * @param color mau sac
     * @param filled trang thai filled
     */
    public Square(double Side, String color, boolean filled, int x, int y)
    {
        super(Side, Side, color,filled, x, y);
    }

    /**
     * Ham in thong tin
     * @return thong tin cua hinh vuong
     */
    @Override
    public String toString()
    {
        return  getLayoutX() + " "+getLayoutY()+" "+ isFilled() + " " +  getColor() + "\n";
    }
}
