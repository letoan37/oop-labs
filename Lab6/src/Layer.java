
/**
 * class Layer
 * @author Le Khanh Toan
 * @version 1.0
 * @since 2018-10-09
 */

import java.util.LinkedList;

public class Layer extends Diagram
{
    protected int  layoutX, layoutY;
    protected LinkedList<Shape> listShapes = new LinkedList<Shape>();

    public void setLayoutY (int layoutY)
    {
        this.layoutY = layoutY;
    }

    public void setLayoutX (int layoutX)
    {
        this.layoutX = layoutX;
    }

    public int getLayoutY ()
    {
        return layoutY;
    }

    public int getLayoutX ()
    {
        return layoutX;
    }

    /**
     * constructor rong
     */
    public Layer(){};


    /**
     * constructor khoi tao layer gom nhieu shape
     * @param list danh sach cac shape
     * @param _width chieu rong
     * @param _height chieu dai
     */
    public Layer(LinkedList<Shape> list, int _width, int _height)
    {
        super(_width, _height);
        listShapes.clear();
        listShapes.addAll(list);
    }

    /**
     * ham xoa cac doi tuong thuoc class Circle
     */
    public void removeAllCircle()
    {
	
        for (int i = 0; i <listShapes.size();i++) {
            if (listShapes.get(i) instanceof Circle) {
                listShapes.remove(i);
            }

        }

    }

    /**
     * ham in thong tin
     * @return tra ve thong tin cua lop layer
     */
    @Override
    public String toString ()
    {
      	String a = "";
        for(int i = 0; i < listShapes.size(); i++) {
            a += listShapes.get(i).toString();

        }
        return a;

    }
}

