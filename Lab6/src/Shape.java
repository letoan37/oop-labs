
/**
 * class Shape ke thua tu class Layer
 * @author Le Khanh Toan
 * @version 1.0
 * @since 2018-10-09
 */

public class Shape extends Layer
{
    private String color;
    private boolean filled;

    public String getColor ()
    {
        return color;
    }

    public void setColor (String color)
    {
        this.color = color;
    }
    public boolean isFilled()
    {
        return this.filled;
    }
    public void setFilled (boolean filled)
    {
        this.filled = filled;
    }

    /**
     * Ham khoi tao mac dinh
     */
    public Shape()
    {
        color = "yellow";
        filled = true;
    }

    /**
     * Ham khoi tao Shape
     * @param _color la mau sac
     * @param _filled la trang thai fill
     * @param  x la toa do x
     * @param y la toa do y
     */
    public Shape(String _color, boolean _filled, int x, int y )
    {
        setColor(_color);
        setFilled(_filled);
        setLayoutX(x);
        setLayoutY(y);
    }
    /**
     * Ham in ra thong tin cua class
     * @return mau cua Shape
     */
    public String toString ()
    {
        return "Position: " + getLayoutX() + " " + getLayoutY() + "\n Checkfilled: " + isFilled() + " Color: "+ getColor() + "\n" ;
    }
}
