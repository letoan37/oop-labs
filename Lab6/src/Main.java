import java.util.LinkedList;
/**
 * class Main khoi tao cac doi tuong
 * @author Le Khanh Toan
 * @version 1.0
 * @since 2018-09-20
 */

public class Main
{
    public static void main (String[] args)
    {

        Shape shape1 = new Triangle(2.0,1.0,3.0,"blue",true,5,5);
        Shape shape2 = new Circle(9.5, "blue", true,10,20);
        Shape shape3 = new Rectangle(1.9,6.1,"green", true,20,10);
        Shape shape4 = new Square(4.0, "red", true,30,10);
        LinkedList<Shape> list01 = new LinkedList<Shape>();
        LinkedList<Shape> list02 = new LinkedList<Shape>();
        list01.add(shape1);
        list01.add(shape2);
        list02.add(shape3);
        list02.add(shape4);
        Layer layer1 = new Layer(list01,200,200);
        Layer layer2 = new Layer(list01,200,400);
        LinkedList<Layer> list03 = new LinkedList<Layer>();
        list03.add(layer1);
        list03.add(layer2);
        Diagram diagram = new Diagram(list03,500,500);
        System.out.printf(shape1.toString());
        System.out.printf(shape2.toString());
        System.out.printf(shape3.toString());
        System.out.println("Layer1: ");
        System.out.println(layer1.toString());
        System.out.println("Layer2: ");
        System.out.println(layer1.toString());
        System.out.println(diagram.toString());
        layer1.removeAllCircle();
        System.out.println("Layer1: ");
        System.out.println(layer1.toString());
        diagram.removeAllTriangle();
        System.out.println(diagram.toString());
    }
}
