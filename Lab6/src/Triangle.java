/**
 * class Triangle ke thua Class Shape
 * @author Le Khanh Toan
 * @version 1.0
 * @since 2018-10-09
 */
public class Triangle extends Shape
{
    private double a, b, c;

    public double geta ()
    {
        return a;
    }

    public double getb ()
    {
        return b;
    }

    public double getc ()
    {
        return c;
    }

    public void seta (double a)
    {
        this.a = a;
    }

    public void setb (double b)
    {
        this.b = b;
    }

    public void setc (double c)
    {
        this.c = c;
    }
    /**
     * Ham khoi tao mac dinh
     */
    public Triangle()
    {
        super();
        a = 1.0;
        b = 1.0;
        c = 1.0;
    }

    /**
     * Ham khoi tao 3 canh
     * @param a canh thu nhat
     * @param b canh thu hai
     * @param c canh thu ba
     */
    public Triangle(double a,double b, double c)
    {
        super();
        seta(a);
        setb(b);
        setc(c);
    }

    /**
     * Ham khoi tao day du thong so color, radius, filled
     * @param a canh thu nhat
     * @param b canh thu hai
     * @param c canh thu ba
     * @param _color mau cua hinh tron
     * @param _filled trang thai fill
     * @param layoutX la toa do X
     * @param layoutY la toa do y
     */
    public Triangle(double a,double b, double c, String _color, boolean _filled, int layoutX, int layoutY)
    {
        super(_color,_filled,layoutX,layoutY);
        seta(a);
        setb(b);
        setc(c);
    }
    /**
     * Ham in thong tin
     * @return thong tin cua HV
     */
    @Override
    public String toString ()
    {
        return "Shape: Triangle\nSide: " + geta() +" "+getb()+" "+getc() +"\n"+ super.toString();
    }
}
