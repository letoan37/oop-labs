/**
 * Chương trình Tìm số Fibonacci thứ n
 * @author Lê Khánh Toàn
 * @version 1.0
 * @since 2018-09-17
 */
 import java.util.Scanner;

public class Fibonacci
{
    /**
    * Fibonacci l phương thức tính số Fibo(n)
    * @param k l so can tinh so Fibonacci
    * @return int phuong thuc Fibonacci tra ve F[n] la so Fibonacci thu n
    */

    public static int Fibonacci(int k) {
        int f0 = 0, f1 = 1, fn = 1;
        if(k < 0) {
            return -1;
        } else if( (k==0) || (k==1) ) {
            return k;
        }else {
            for( int i=2; i<k; i++) {
                f0 = f1;
                f1 = fn;
                fn = f0 + f1;
            }
        }
        return fn;
    }

    public static void main(String[] args) {
        int n;
        System.out.printf("Nhap vao so n: ");
        Scanner a = new Scanner(System.in);
        n = a.nextInt();
        if (n < 0) {
            System.out.printf("Error!");
        }
        System.out.println("Dãy Fibonacci: ");
        for(int i=0; i < n; i++) {
            System.out.print(String.valueOf(Fibonacci(i))+"\t");
        }
    }
}
