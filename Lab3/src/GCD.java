/**
 * Chương trình tìm UCLN của 2 số
 * @author Lê Khánh Toàn
 * @version 1.0
 * @since 2018-09-17
 */
import java.util.Scanner;
public class GCD
{
	/**
	 * GCD là phương thức tính UCLN của 2 số nguyên
	 * @param number1 là số thứ 1
	 * @param number2 là số thứ 2
	 * @return GCD
	 */
	 public int GCD(int number1, int number2)
	 {
//		if (number1 == number2 || number1 == -number2) return Math.abs(number1);
//		if (number1 == 0 || number2 == 0) return Math.abs(number1 + number2);
//		return GCD(number1 % number2,number2 % number1);
        if (number1 < 0 || number2 < 0) {
            number1 = Math.abs(number1);
            number2 = Math.abs(number2);
        }
        while (number1 != number2) {
            if (number1 > number2) {
                 number1 -= number2;
            } else {
                 number2 -= number1;
            }
        }
        return  number1;
	 }

    	 public static void main(String[] args)
    	 {
		System.out.printf("Nhap vao 2 so can tinh UCLN: ");
		Scanner sc = new Scanner(System.in);
		int number1, number2, ulcn;
		GCD check = new GCD();
		number1 = sc.nextInt();
		number2 = sc.nextInt();
		ulcn = check.GCD(number1,number2);
		System.out.print(ulcn);

    	}
}
