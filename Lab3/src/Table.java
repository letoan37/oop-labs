public class Table
{
    private int  Length, Width, Height;


    public int getLength()
    {
        return Length;
    }

    public int getWidth()
    {
        return Width;
    }

    public void setLength(int Length)
    {
        this.Length = Length;
    }

    public void setWidth(int Width)
    {
        this.Width = Width;
    }
    public void setHeight(int Height)
    {
        this.Height = Height;
    }

    public int getHeight()
    {
        return Height;
    }

}
