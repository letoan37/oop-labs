public class Dog
{
    private String Type;
    private int Height, Weight, Old;

    public int getHeight()
    {
        return Height;
    }
    public int getWeight()
    {
        return Weight;
    }
    public int getOld()
    {
        return Old;
    }


    public String getType()
    {
        return Type;
    }

    public void setHeight(int Height)
    {
        this.Height = Height;
    }

    public void setOld(int Old)
    {
        this.Old = Old;
    }

    public void setType(String Type)
    {
        this.Type = Type;
    }

    public void setWeight(int Weight)
    {
        this.Weight = Weight;
    }
}
