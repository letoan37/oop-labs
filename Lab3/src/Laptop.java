public class Laptop
{   
    private int Size, Price;
    private String Type;

    public int getSize() {
        return Size;
    }

    public void setType(int Size) {
        this.Size = Size;
    }

    public void setPrice(int Price) {
        this.Price = Price;
    }

    public int getPrice() {
        return Price;
    }

    public String getType() {
        return Type;
    }

    public void setType(String Type) {
        this.Type = Type;
    }

}
