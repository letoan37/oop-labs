public class Mobile
{
    private int Size, Price;

    private String Type;

    public void setSize(int Size)
    {
        this.Size = Size;
    }

    public int getSize()
    {
        return Size;
    }

    public int getPrice()
    {
        return Price;
    }

    public void setPrice(int Price)
    {
        this.Price = Price;
    }
    public void setType(String Type)
    {
        this.Type = Type;
    }

    public String getType()
    {
        return Type;
    }
}
