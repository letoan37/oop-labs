/**
 * chuong trinh thao tac tren phan so(cong, tru, nhan, chia, so sanh)
 * @author Le Khanh Toan
 * @version 1.0
 * @since 2018-09-17
 */
public class PhanSo
{
    private int tuso,mauso;

    public void setTuso(int x)
    {
        this.tuso = x;
    }

    public void setMauso(int y)
    {
        this.mauso = y;
    }

    public int getTuso()
    {
        return this.tuso;
    }

    public int getMauso()
    {
        return this.mauso;
    }
    /**
     * PhanSo la phuong thuc khoi tao phan so
     * @param x la tu so
     * @param y la mau so
     */
    public void PhanSo(int x, int y)
    {
        this.setTuso(x);
        this.setMauso(y);

    }
    /**
     * simplify la phuong thuc toi gian 1 phan so
     */
    public void simplify()
    {
        GCD check = new GCD();
        int a = this.getTuso();
        int b = this.getMauso();
        if (a < 0 && b < 0) {
            a = a / (-1);
            b = b / (-1);
        } else if (( a < 0 && b > 0) || ( a > 0 && b < 0)) {
            b = Math.abs(b);
            a = Math.abs(a) * (-1);
        } else { a = a;
            b = b;
        }
        if((a<0 && b<0) || (a>0 && b <0)) {
            a = -a;
            b = -b;
        }
        this.setTuso(a / (check.GCD(this.getMauso(),this.getTuso())));
        this.setMauso(b / (check.GCD(this.getMauso(),this.getTuso())));
    }
    /**
     * add la ham cong 2 phan so
     * @param a la phan so duoc cong them
     * @return PhanSo tra ve phan so ket qua la tong 2 phan so
     */
    public PhanSo add(PhanSo a)
    {
        a.setMauso(a.getMauso() * this.getMauso() );
        a.setTuso(a.getTuso() * this.getMauso() + this.getTuso() * a.getMauso());
        a.simplify();
        return a;



    }
    /**
     * sub la ham tru 2 phan so
     * @param a la phan so tru
     * @return PhanSo tra ve phan so ket qua la hieu 2 phan so
     */
    public PhanSo sub(PhanSo a)
    {
        a.setMauso(a.getMauso()*this.getMauso());
        a.setTuso(-a.getTuso()*this.getMauso()+this.getTuso()*a.getMauso());
        a.simplify();
        return a;
    }
    /**
     * mul la ham nhan 2 phan so
     * @param a la phan so duoc nhan
     * @return PhanSo tra ve phan so ket qua la tich 2 phan so
     */
    public PhanSo mul (PhanSo a)
    {
        a.setTuso(a.getTuso()*this.getTuso());
        a.setMauso(a.getMauso()*this.getMauso());
        a.simplify();
        return a;
    }
    /**
     * div la ham chia 2 phan so
     * @param a la phan so chia
     * @return PhanSo tra ve phan so ket qua la thuong 2 phan so
     */
    public PhanSo div (PhanSo a)
    {
        a.setMauso(a.getTuso()*this.getMauso());
        a.setTuso(a.getMauso()*this.getTuso());
        a.simplify();
        return a;
    }
    /**
     * equals la ham kiem tra 2 phan so bang nhau
     * @param a la phan so can kiem tra
     * @return boolean tra ve so ket qua bang hoac ko bang cua 2 phan so
     */
    public boolean equals(PhanSo a)
    {
        a.simplify();
        this.simplify();
        if (a.getTuso() == this.getTuso() && a.getMauso()==this.getMauso()) {
            return true;
        }
        return false;
    }


    public static void main(String[] args)
    {
        PhanSo a = new PhanSo();
        PhanSo b = new PhanSo();
        a.PhanSo(5,2);
        b.PhanSo(-3,-2);
        System.out.println(a.sub(b));
        System.out.println(a.div(b));
        System.out.println(a.mul(b));
        System.out.println(a.add(b));


    }
}
