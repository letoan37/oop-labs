public class Teacher
{
    private String name, sex, major;
    private int weight, height, old;

    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }


    public int getWeight()
    {
        return weight;
    }
    public int getHeight()
    {
    return height;
    }
    public int getOld()
    {
        return old;
    }

    public void setWeight(int weight)
    {
        this.weight = weight;
    }
    public void setHeight(int height)
    {
        this.height = height;
    }

    public void setOld(int old)
    {
        this.old = old;
    }

    public String getMajor()
    {
        return major;
    }

    public String getSex()
    {
        return sex;
    }

    public void setMajor(String major)
    {
        this.major = major;
    }

    public void setSex(String sex)
    {
        this.sex = sex;
    }

}
