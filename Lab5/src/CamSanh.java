/**
 * class CamSanh Phong ke thua tu class QuaCam
 * @author Le Khanh Toan
 * @version 1.0
 * @since 2018-09-20
 */
public class CamSanh extends QuaCam
{
    /**
     * constructor
     * @param price
     * @param quantity
     */
    public CamSanh(int price, int quantity)
    {
        super(price,quantity,"hanoi");
    }
    /**
     * toString la ham lay thong tin cua class
     * @return String tra ve xau chua thong tin
     */
    @Override
    public String toString ()
    {
        return "quantity: " + this.quantity + " price: " + this.price;
    }
}
