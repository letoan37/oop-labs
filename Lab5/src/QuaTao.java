/**
 * class QuaTao ke thua tu class HoaQua
 * @author le Khanh Toan
 * @version 1.0
 * @since 2018-09-20
 */
public class QuaTao extends HoaQua
{
    /**
     * constructor
     * @param price
     * @param quantity
     */
    public QuaTao(int price,int quantity)
    {
        super(price,quantity);
    }
    /**
     * toString la ham lay thong tin cua class
     * @return String tra ve xau chua thong tin gom: quantity, price
     */
    @Override
    public String toString ()
    {
        return "quantity: " + this.quantity + " price: " + this.price + "\n";
    }
}
