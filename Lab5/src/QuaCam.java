/**
 * class QuaCam ke thua tu class HoaQua
 * @author Le Khanh Toan
 * @version 1.0
 * @since 2018-09-20
 */
public class QuaCam extends HoaQua
{
    protected String nsx;

    /**
     * constructor
     * @param price
     * @param quantity
     */
    public QuaCam(int price, int quantity, String place)
    {
        super(price,quantity);
        this.nsx = place;
    }
}
