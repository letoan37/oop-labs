/**
 * class HoaQua gom cac thuoc tinh chung
 * @author Le Khanh toan
 * @version 1.0
 * @since 2018-09-20
 */
public class HoaQua
{
    protected int price, quantity;
    /**
     * constructor
     * @param price
     * @param quantity
     */
    public HoaQua(int price, int quantity)
    {
        this.price = price;
        this.quantity = quantity;
    }
}
