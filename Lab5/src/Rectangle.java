/**
 * class Rectangle ke thua tu class Shape
 * @author Le Khanh Toan
 * @version 1.0
 * @since 2018-10-01
 */
public class Rectangle extends Shape
{
    private double width, length;

    public double getLength ()
    {
        return length;
    }

    public void setLength (double length)
    {
        this.length = length;
    }

    public double getWidth ()
    {
        return width;
    }

    public void setWidth (double width)
    {
        this.width = width;
    }

    /**
     * Ham khoi tao mac dinh
     */
    public Rectangle()
    {
        super();
        width = 1.0;
        length = 1.0;
    }

    /**
     * Ham khoi tao chi co chieu dai, rong
     * @param _width chieu rong hcn
     * @param _length chieu dai hcn
     */
    public Rectangle(double _width, double _length)
    {
        super();
        setLength(_length);
        setWidth(_width);
    }

    /**
     * Ham khoi tao day du tham so truyen vao
     * @param _width chieu rong hcn
     * @param _length chieu dai hcn
     * @param _color mau sac hcn
     * @param _filled trang thai fill
     */
    public Rectangle(double _width, double _length, String _color, boolean _filled)
    {
        super(_color,_filled);
        setWidth(_width);
        setLength(_length);
    }

    /**
     * Ham tinh dien tich hcn
     * @return double dien tich hcn
     */
    public double getArea()
    {
        return getLength()*getWidth();
    }

    /**
     * Ham tinh chu vi hcn
     * @return double chu vi hcn
     */
    public double getPerimeter()
    {
        return 2*(getWidth() + getLength());
    }

    /**
     * Ham in thong tin hcn
     * @return String la thong tin gom chieu dai, rong, mau sac, loai hinh
     */
    @Override
    public String toString ()
    {
        return "Shape: Retangle\nWidth: " + getWidth() + "\nLength: " + getLength() + "\n" + super.toString();
    }
}
