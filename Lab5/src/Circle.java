/**
 * class Circle ke thua tu class Shape
 * @author Le Khanh Toan
 * @version 1.0
 * @since 2018-10-01
 */

public class Circle extends Shape
{
    private double radius;
    public static final double pi = 3.14;
    public double getRadius ()
    {
        return radius;
    }

    public void setRadius (double radius)
    {
        this.radius = radius;
    }

    /**
     * Ham khoi tao mac dinh
     */
    public Circle()
    {
        super();
        radius = 1.0;
    }

    /**
     * Ham khoi tao chi co ban kinh
     * @param _radius ban kinh hinh tron
     */
    public Circle(double _radius)
    {
        super();
        setRadius(_radius);
    }

    /**
     * Ham khoi tao day du thong so color, radius, filled
     * @param _radius ban kinh hinh tron
     * @param _color mau cua hinh tron
     * @param _filled trang thai fill
     */
    public Circle(double _radius, String _color, boolean _filled)
    {
        super(_color,_filled);
        setRadius(_radius);
    }

    /**
     * Ham tinh dien tich hinh tron
     * @return double dien tich
     */
    public double getArea()
    {
        return pi * getRadius() * getRadius();
    }

    /**
     * Ham tinh chu vi hinh tron
     * @return double chu vi
     */
    public double getPerimeter()
    {
        return 2 * pi * getRadius();
    }

    /**
     * Ham in ra thong tin hinh tron
     * @return thong tin gom mau sac, ban kinh, hinh dang
     */
    @Override
    public String toString()
    {
        return "Shape: Circle\nRadius: " + getRadius() + "\n"+ super.toString();
    }
}
