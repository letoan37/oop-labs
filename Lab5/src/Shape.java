/**
 * class Shape
 * @author Le Khanh Toan
 * @version 1.0
 * @since 2018-10-01
 */

public class Shape
{
    private boolean filled;
    private String color;


    public String getColor ()
    {
        return color;
    }
    public boolean isFilled()
    {
        return this.filled;
    }
    public void setColor (String color)
    {
        this.color = color;
    }

    public void setFilled (boolean filled)
    {
        this.filled = filled;
    }

    /**
     * Ham khoi tao mac dinh
     */
    public Shape()
    {
        color = "red";
        filled = true;
    }

    /**
     * Ham khoi tao Shape
     * @param _color la mau sac
     * @param _filled la trang thai fill
     */
    public Shape(String _color, boolean _filled)
    {
        setColor(_color);
        setFilled(_filled);
    }

    /**
     * Ham in ra thong tin cua class
     * @return mau cua Shape
     */
    public String toString ()
    {
        return "Color: " + getColor() + "\n";
    }
}
