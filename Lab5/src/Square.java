/**
 * class Square ke thua tu class Rectangle
 * @author Le Khanh Toan
 * @version 1.0
 * @since 2018-10-01
 */

public class Square extends Rectangle
{
    public double getSide()
    {
        return getWidth();
    }
    public void setSide(double Side)
    {
        setLength(Side);
        setWidth(Side);
    }
    @Override
    public void setLength(double Side)
    {
        super.setLength(Side);
        super.setWidth(Side);
    }
    @Override
    public void setWidth(double Side)
    {
        super.setWidth(Side);
        super.setLength(Side);
    }
    /**
     * Constructor rong
     */
    public Square()
    {
        super();
    }

    /**
     * Constructor
     * @param Side canh cua hinh vuong
     */
    public Square(double Side)
    {
        super(Side,Side);
    }

    /**
     * constructor
     * @param Side canh cua hv
     * @param color mau sac
     * @param filled trang thai filled
     */
    public Square(double Side, String color, boolean filled)
    {
        super(Side,Side,color,filled);
    }

    /**
     * Ham in thong tin
     * @return thong tin cua HV
     */
    @Override
    public String toString()
    {
        return "Shape: Square \nSide: " + getSide() +"\nColor: " +  getColor() + "\n";
    }
}
